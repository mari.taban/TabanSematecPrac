package taban.practisesematecclass;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Acer on 6/22/2017.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    String weatherTBL = "" +
            " CREATE TABLE weatherta ( " +
            " _ID INTEGER AUTO INCREMENT PRIMARY KEY ," +
            " temp TEXT ,"+
            " hum TEXT ," +
            " wid TEXT ," +
            " dat TEXT " +
            ") " +
            "";

    public DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(weatherTBL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void insertWeather(String temp, String hum, String wid, String dat) {
        String insertQuery = "" +
                " INSERT INTO weatherta ( temp, hum, wid, dat ) " +
                " VALUES( '" + temp + "' , '" + hum + "' , '" + wid + "' , '" + dat + "' )";
       // " VALUES(      '" + name + "' , '" + family + "' , '" + mobile + "' )";
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.execSQL(insertQuery);
            Log.d("InsertDB",insertQuery.toString());

        }
        catch (Exception e) {

        }


        db.close();
    }

    public String getWeather() {
        String results = "";
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM weatherta", null);
//        Cursor cursor = db.rawQuery("SELECT temp,hum,wid,dat FROM weather2", null);
//     cursor.moveToFirst();
        Log.d("EventsCursor",Integer.toString(cursor.getCount()));
       // int s = cursora.getCount();
        while (cursor.moveToNext()) {
            results +=
                    cursor.getString(0) +
                            " " +
                            cursor.getString(1) +
                            " " +
                            cursor.getString(2) +
                            " " +
                            cursor.getString(3) +
                            "\n";


        };



        db.close();
        return results;
    }
}
