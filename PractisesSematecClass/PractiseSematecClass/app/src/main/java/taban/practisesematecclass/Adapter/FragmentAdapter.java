package taban.practisesematecclass.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import taban.practisesematecclass.fragments.FragmentA;
import taban.practisesematecclass.fragments.FragmentB;

/**
 * Created by Acer on 7/5/2017. for view pager
 */

public class FragmentAdapter extends FragmentPagerAdapter {
    public FragmentAdapter(FragmentManager fm)
    {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0)
            return FragmentA.getInstance();
        if (position == 1)
            return FragmentB.getInstance();

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0)
            return "Today Weather";
        if (position == 1)
            return "Tomorrow Weather";

        return null;

    }
}