package taban.practisesematecclass;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import taban.practisesematecclass.Adapter.FragmentAdapter;

public class PagerActivity extends AppCompatActivity {
    ViewPager myPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pager);


        /////
        myPager = (ViewPager) findViewById(R.id.myPager);
        FragmentAdapter adapter = new FragmentAdapter(getSupportFragmentManager());
        myPager.setAdapter(adapter);
//        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
//        tabs.setViewPager(myPager);
        /////

    }
}
