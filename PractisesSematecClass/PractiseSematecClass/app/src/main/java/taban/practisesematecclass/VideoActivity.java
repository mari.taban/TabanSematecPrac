package taban.practisesematecclass;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class VideoActivity extends AppCompatActivity {
    VideoView myVideo;
    String url = "http://as5.asset.aparat.com/aparat-video/02bccaa612d63834cbbb9b2bef8e96387417652-144p__63165.mp4";
    BroadcastReceiver callReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        if (ContextCompat.checkSelfPermission(this,Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[]
                            {Manifest.permission.READ_PHONE_STATE},500);
        }
        callReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (myVideo.isPlaying())
                    myVideo.pause();
            }
        };
        myVideo = (VideoView) findViewById(R.id.myVideo);
        myVideo.setMediaController(new MediaController(this));
        myVideo.setVideoURI(Uri.parse(url));
        myVideo.start();
        IntentFilter callRecIntent = new IntentFilter("android.intent.action.PHONE_STATE");
        registerReceiver(callReceiver, callRecIntent);
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(callReceiver);
    }
}
