package taban.practisesematecclass;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

//import com.google.gson.Gson;
//import taban.practisesematecclass.Models.weather.WeatherModel;

public class CallWebServiceActivity extends AppCompatActivity {
    TextView todaytemp;
    TextView humidity;
    TextView wind;
    TextView todaydate;
    DatabaseHandler handler;
    TextView ShowData;
    Button show;
    Button save;
//    String temp ;
//    String hu;
//    String wd ;
//    String ts ;
//    ImageView ImageWeather;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_web_service);
        todaytemp = (TextView) findViewById(R.id.TodayTemp);
        humidity = (TextView) findViewById(R.id.Humidity);
        wind = (TextView) findViewById(R.id.Wind);
        ShowData = (TextView) findViewById(R.id.ShowData);
        show = (Button) findViewById(R.id.Show);
        save = (Button) findViewById(R.id.Save);
        getTemp();
        handler = new DatabaseHandler(this, "sematectb.db", null, 1);

//        findViewById(R.id.Button).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                getTemp();
//            }
//        });

    }

    void getTemp() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
//                    URL obj = new URL("http://api.openweathermap.org/data/2.5/forecast?id=112931&APPID=0301bcf185e175f8c56ab1e85a97efa0");
                    URL obj = new URL("http://api.airvisual.com/v2/nearest_city?lat=35&lon=51&rad=2000&key=syNvfod3auzSqzLTK");
                    HttpURLConnection con = null;
                    con = (HttpURLConnection) obj.openConnection();
                    con.setRequestMethod("GET");
                    con.setRequestProperty("User-Agent", "Mozilla/5.0");
                    int responseCode = con.getResponseCode();
                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();
                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }
                        parseByGson(response.toString());
                      //  parseServerResponse(response.toString());
//                        System.out.println(response.toString());
                    }

                } catch (IOException e) {
//                    e.printStackTrace();
                    Toast.makeText(CallWebServiceActivity.this, "Error in connecting to website", Toast.LENGTH_SHORT).show();
                }

            }
        }).start();

    }

    void parseByGson(String response) {
        String out;
        //Gson gson = new Gson();
       // WeatherModel weatherm = gson.fromJson(response , WeatherModel.class);
       //  out = weatherm.getList().get(0).getMain().getTemp().toString();
       // todaytemp.setText(out);
//        todaydate.setText(weatherModel.getCity().getName());

    }

    void parseServerResponse(String response) {

        Log.d("webservice_debug", response);
        try {
            JSONObject allobj = new JSONObject(response);
            String datastr = allobj.getString("data");

            JSONObject dataobj = new JSONObject(datastr);
            String currstr = dataobj.getString("current");
            JSONObject currobj = new JSONObject(currstr);
            String weatherstr = currobj.getString("weather");
            JSONObject weatherstrobj = new JSONObject(weatherstr);
            final String temp = weatherstrobj.getString("tp");
            final String hu = weatherstrobj.getString("hu");
            final String wd = weatherstrobj.getString("wd");
            final String ts = weatherstrobj.getString("ts");
            final String fitemp = temp + " 'C";
            final String fihu = "Humidity: " + hu;
            final String fiwd = "Wind: " + wd;
            final String fidat = ts;


//            JSONObject allobj = new JSONObject(response);
//            String liststr = allobj.getString("list");

//            JSONObject allobj2 = new JSONObject(response); *
//            String liststr2 = allobj2.getString("city");*
//            JSONObject all = new JSONObject(liststr2);*
//            String listsr = allobj.getString("country");*
//            final String temp = all.getString("country");*

//            JSONObject listobj = new JSONObject(liststr);
//            String zerostr = listobj.getString("0");
//
//            JSONObject zeroobj = new JSONObject(zerostr);
//            String mainstr = zeroobj.getString("main");
//            JSONObject tempobj = new JSONObject(mainstr);
//            final String temp = tempobj.getString("temp");
//            final String date = zeroobj.getString("dt_txt");
//
//            save data in database
//            handler = new DatabaseHandler(this,"sematectdb.db", null , 1);
//             handler.insertWeather( temp.toString() , hu.toString() ,wd.toString() , ts.toString() );

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

//                    todaytemp.setText(temp.toString() + " 'C");
                    todaytemp.setText(fitemp);
                    humidity.setText(fihu);
                    wind.setText(fiwd);
//                    todaydate.setText(fidat);

                    //insert
                    save.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            handler.insertWeather(fitemp, fihu, fiwd, "hh");
                            Toast.makeText(CallWebServiceActivity.this, "Data has been saved", Toast.LENGTH_SHORT).show();
                        }
                    });
//                    // show data from DB - get result
                    show.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String result = handler.getWeather();
                            ShowData.setText(result);

                        }
                    });
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}
