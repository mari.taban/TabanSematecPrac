package taban.practisesematecclass;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import taban.practisesematecclass.Adapter.FragmentAdapter;
import taban.practisesematecclass.Models.weather.WeatherModel;


public class CallWebServiceGsonAsyncActivity extends AppCompatActivity {
    TextView todaytemp;
    TextView humidity;
    TextView wind;
    TextView todaydate;
    DatabaseHandler handler;
    TextView ShowData;
    Button show;
    Button save;
    String url;
    String temp;
    String hu;
    String wd;
    String dat;
   // private ProgressDialog waiting;
   ViewPager myPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_web_service);
        /////
        myPager = (ViewPager) findViewById(R.id.myPager);
        FragmentAdapter adapter = new FragmentAdapter(getSupportFragmentManager());
        myPager.setAdapter(adapter);
//        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
//        tabs.setViewPager(myPager);
        /////

        todaytemp = (TextView) findViewById(R.id.TodayTemp);
        todaydate = (TextView) findViewById(R.id.TodayDate);
        humidity = (TextView) findViewById(R.id.Humidity);
        wind = (TextView) findViewById(R.id.Wind);

        show = (Button) findViewById(R.id.Show);
        save = (Button) findViewById(R.id.Save);
        ShowData = (TextView) findViewById(R.id.ShowData);
        url = "http://api.openweathermap.org/data/2.5/forecast?id=112931&APPID=0301bcf185e175f8c56ab1e85a97efa0";
//
//        findViewById(R.id.Show).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
        showNotification();
        getTemp(url);
        handler = new DatabaseHandler(this, "sematecdba.db", null, 1);
//        waiting = new ProgressDialog(this);
//        waiting.setMessage("Please wait");
//            }
//        });

        //insert
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handler.insertWeather(temp, hu, wd, dat);
                Toast.makeText(CallWebServiceGsonAsyncActivity.this, "Data has been saved", Toast.LENGTH_LONG).show();
            }
        });

        // show data from DB - get result
        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String result = handler.getWeather();
                ShowData.setText(result);

            }
        });

    }


    void getTemp(String url) {

//        waiting.show();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int i, cz.msebera.android.httpclient.Header[] headers, String s, Throwable throwable) {
                Toast.makeText(CallWebServiceGsonAsyncActivity.this, "Connection fialed", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onSuccess(int i, cz.msebera.android.httpclient.Header[] headers, String s) {
                Gson gson = new Gson();
                WeatherModel wmodel;
                Log.d("webservice_debug1", s);
                if (i == 200 && s != null) {
                    wmodel = gson.fromJson(s, WeatherModel.class);
                    Log.d("webservice_debug2", wmodel.toString());

                    double Dtemp = Double.parseDouble(wmodel.getList().get(0).getMain().getTemp().toString());
                    Double Ctemp;
                    //Ctemp = (( temp - 32) * 5/9  ) ;
                    Ctemp = ( Dtemp / 10 );
                    temp = String.valueOf(Ctemp.intValue());
                    hu = wmodel.getList().get(0).getMain().getHumidity().toString();
                    wd = wmodel.getList().get(0).getWind().getSpeed().toString();
                    dat = wmodel.getList().get(0).getDtTxt().toString();

                    todaytemp.setText(temp);
                    humidity.setText(hu);
                    wind.setText(wd);
                    todaydate.setText(dat);

                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
//                waiting.dismiss();
            }
        });

    }



    void showNotification() {
        Intent intent = new Intent("notificationTest");
        intent.putExtra("name", "key1");
        PendingIntent pIntent = PendingIntent.getBroadcast(this, (int) System.currentTimeMillis(), intent, 0);
        Notification notif = new NotificationCompat.Builder(this)
                .setContentTitle("Write title")
                .setContentText("This is a test for notification in weather app")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pIntent)
                .setAutoCancel(true)
                .addAction(R.mipmap.ic_launcher, "Call", pIntent)
                .addAction(R.mipmap.ic_launcher, "More", pIntent)
                .addAction(R.mipmap.ic_launcher, "Remove", pIntent).build();
        NotificationManager notificationCompatManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationCompatManager.notify((int) System.currentTimeMillis(), notif);

    }
}

