package taban.practisesematecclass.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import taban.practisesematecclass.R;

/**
 * Created by Acer on 7/5/2017.
 */

public class FragmentB extends Fragment {
    public static FragmentB fragment;
    private TextView myText;

    public static FragmentB getInstance() {
        if (null == fragment)
            fragment = new FragmentB();
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myView = inflater.inflate(R.layout.fragment_b, container, false);

        myText = (TextView) myView.findViewById(R.id.myText);

        return myView;
    }
}
